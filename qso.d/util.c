/*
 ********************
 * Utility routines *
 ********************
 */
#include <stdlib.h>

#include "qso.h"

extern int their_age;

int CountStrings(const char *StringVector[])
/*
 * Count the number of string values in the supplied vector
 * of pointers. Start with the first pointer and stop when
 * NIL (0) is encountered.
 */
{
	register const char **SV;
	register int Count;

	Count = 0;

	for (SV = StringVector; *SV; SV++) {
		Count++;
	}
	return (Count);
}

int Roll(int Number) {
	// (ESR) This is technically incorrect but Number is always small enough
	// compared to RAND_MAX that we don't care.
	return (rand() % Number);
}

int License_Seed(void) {
	if (their_age > 20) {
		return (20);
	}
	if (their_age < 10) {
		return (10);
	}
	return (their_age - 8);
}

char const *Choose(char const *Words[], int Number) {
	return (Words[Roll(Number)]);
}
